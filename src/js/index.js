const menuWrapper = document.querySelector('.header-menu-wrapper');
const menu = document.querySelector('.header-menu');
const item = document.querySelectorAll('.header-menu-item');
const link = document.querySelectorAll('.header-menu-link');
const logo = document.querySelector('.menu-logo');
const header = document.querySelector('.header-container');
const closeBtn = document.querySelector('.close-btn');


logo.addEventListener('click',()=>{
  logo.classList.add('inactive');
  closeBtn.classList.remove('inactive');
    
    header.insertAdjacentHTML("afterend",`<div class="menu-mob-wrapper" >
    <nav>
      <ul class="menu-mob" >
        <li class="menu-mob-item">
          <a href="#" class="menu-mob-link">Overview</a>
        </li>
        <li class="menu-mob-item">
          <a href="#" class="menu-mob-link">About Fork</a>
        </li>
        <li class="menu-mob-item">
          <a href="#" class="menu-mob-link">Buying Options</a>
        </li>
        <li class="menu-mob-item">
          <a href="#" class="menu-mob-link">Support</a>
        </li>
      </ul>
    </nav>
  </div>`)
});


